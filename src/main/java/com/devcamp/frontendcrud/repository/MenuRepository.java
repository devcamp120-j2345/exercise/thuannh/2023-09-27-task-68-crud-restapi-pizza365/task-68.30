package com.devcamp.frontendcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.frontendcrud.model.CMenu;

public interface MenuRepository extends JpaRepository<CMenu, Long>{
    CMenu findById(int id);
    CMenu findByCombo(String combo);
}
