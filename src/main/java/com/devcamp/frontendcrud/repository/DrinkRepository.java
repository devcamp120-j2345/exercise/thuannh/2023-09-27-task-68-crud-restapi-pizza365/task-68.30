package com.devcamp.frontendcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.frontendcrud.model.Drink;

public interface DrinkRepository extends JpaRepository<Drink, Long>  {
    
}
