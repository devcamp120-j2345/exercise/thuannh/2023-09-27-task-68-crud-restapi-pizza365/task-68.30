package com.devcamp.frontendcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.frontendcrud.model.CCountry;

public interface CountryRepository extends JpaRepository<CCountry, Long>{
    CCountry findByCountryCode(String countryCode);
}


